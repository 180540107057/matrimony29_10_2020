package com.ketan.matrimony29_11_2020.model;

import java.io.Serializable;

public class CityModel implements Serializable {

    int CityId;
    String CityName;

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }
}
