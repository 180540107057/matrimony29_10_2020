package com.ketan.matrimony29_11_2020.model;

import java.io.Serializable;

public class LanguageModel implements Serializable {

    int LanguageId;
    String LanguageName;

    public int getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(int languageId) {
        LanguageId = languageId;
    }

    public String getLanguageName() {
        return LanguageName;
    }

    public void setLanguageName(String languageName) {
        LanguageName = languageName;
    }

}
