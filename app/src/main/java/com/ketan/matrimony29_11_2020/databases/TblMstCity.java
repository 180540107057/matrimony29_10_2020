package com.ketan.matrimony29_11_2020.databases;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ketan.matrimony29_11_2020.model.CityModel;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class TblMstCity extends MyDataBase {
    public static final String TABLE_NAME ="TblMstCity";
    public static final String CITY_ID ="CityId";
    public static final String CITY_NAME ="CityName";



    public TblMstCity(Context context) {
        super(context);
    }

    public ArrayList<CityModel> getCityList()
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<CityModel>cityList = new ArrayList<>();
        String query = "SELECT * FROM "+TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        CityModel cityModel1 = new CityModel();
        cityModel1.setCityName("Select One");
        cityList.add(0,cityModel1);
        for(int i = 0;i<cursor.getCount();i++)
        {
            CityModel cityModel = new CityModel();
            cityModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
            cityList.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return cityList;
    }
}
