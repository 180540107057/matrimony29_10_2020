package com.ketan.matrimony29_11_2020.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDataBase extends SQLiteAssetHelper {

    public static final String DATABASE_NAME = "MatrimonyDataBase.db";
    public static final int DATABASE_VERSION = 1;
    public MyDataBase(Context context ) {
        super(context, DATABASE_NAME,  null, DATABASE_VERSION);
    }
}
