package com.ketan.matrimony29_11_2020.databases;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ketan.matrimony29_11_2020.model.CityModel;
import com.ketan.matrimony29_11_2020.model.LanguageModel;

import java.util.ArrayList;

public class TblMstLanguage extends MyDataBase
{
    public static final String TABLE_NAME ="TblMstLanguage";
    public static final String LANGUAGE_ID ="LanguageId";
    public static final String LANGUAGE_NAME ="LanguageName";
    public TblMstLanguage(Context context) {
        super(context);
    }
    public ArrayList<LanguageModel> getLanguageList()
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<LanguageModel>languageList = new ArrayList<>();
        String query = "SELECT * FROM "+TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        LanguageModel languageModel1 = new LanguageModel();
        languageModel1.setLanguageName("Select one");
        languageList.add(0,languageModel1);
        cursor.moveToFirst();
        for(int i = 0;i<cursor.getCount();i++)
        {
           LanguageModel languageModel = new LanguageModel();
           languageModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
           languageModel.setLanguageName(cursor.getString(cursor.getColumnIndex(LANGUAGE_NAME)));
           cursor.moveToNext();
           languageList.add(languageModel);
        }
        cursor.close();
        db.close();
        return languageList;
    }

}
