package com.ketan.matrimony29_11_2020.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ketan.matrimony29_11_2020.model.UserModel;

import java.util.ArrayList;

public class TblUser extends MyDataBase {

    public static  final String TABLE_NAME = "TblUser";
    public static final String USER_ID = "UserId";
    public static  final String NAME_OF_USER = "Name";
    public static  final String FATHER_NAME = "FatherName";
    public static  final String SUR_NAME = "SurName";
    public static  final String PHONE_NUMBER = "PhoneNumber";
    public static  final String DATE_OF_BIRTH = "Dob";
    public static  final String IS_FAVORITE = "ISFavorite";
    public static  final String EMAIL_ADDRESS = "EmailAddress";
    public static final String GENDER = "Gender";
    public static  final String CITY_ID = "CityId";
    public static final String LANGUAGE_ID = "LanguageId";
    public static final String LANGUAGE_NAME = "LanguageName";
    public static final String CITY_NAME = "CityName";





    public TblUser(Context context) {
        super(context);
    }

    public UserModel getModelUsingCursor(Cursor cursor)
    {
        UserModel userModel = null;
        userModel.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        userModel.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
        userModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
        userModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        userModel.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        userModel.setName(cursor.getString(cursor.getColumnIndex(NAME_OF_USER)));
        userModel.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
        userModel.setDob(cursor.getString(cursor.getColumnIndex(DATE_OF_BIRTH)));
        userModel.setEmailAddress(cursor.getColumnName(cursor.getColumnIndex(EMAIL_ADDRESS)));
        userModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        userModel.setCityName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
        userModel.setLanguageName(cursor.getString(cursor.getColumnIndex(LANGUAGE_NAME)));
        return userModel;

    }

    public long insertUser(String name,String fatherName,String surName,String phoneNumber,
                           String emailAddress,String dob,int gender,int cityId,int languageId)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME_OF_USER,name);
        cv.put(SUR_NAME,surName);
        cv.put(FATHER_NAME,fatherName);
        cv.put(PHONE_NUMBER,phoneNumber);
        cv.put(EMAIL_ADDRESS,emailAddress);
        cv.put(DATE_OF_BIRTH,dob);
        cv.put(GENDER,gender);
        cv.put(CITY_ID,cityId);
        cv.put(LANGUAGE_ID,languageId);
        long lastInsertedId = db.insert(TABLE_NAME,null,cv);
        db.close();
        return lastInsertedId;
    }

    public int updateUser(int userId,String name,String fatherName,String surName,String phoneNumber,
                           String emailAddress,String dob,int gender,int cityId,int languageId)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_ID,userId);
        cv.put(NAME_OF_USER,name);
        cv.put(SUR_NAME,surName);
        cv.put(FATHER_NAME,fatherName);
        cv.put(PHONE_NUMBER,phoneNumber);
        cv.put(EMAIL_ADDRESS,emailAddress);
        cv.put(DATE_OF_BIRTH,dob);
        cv.put(GENDER,gender);
        cv.put(CITY_ID,cityId);
        cv.put(LANGUAGE_ID,languageId);
        int updatedUserId = db.update(TABLE_NAME,cv, USER_ID+" = ?",new String[]{String.valueOf(userId)});
        db.close();
        return updatedUserId;
    }

    public int deletedUserById(int userId)
    {
        SQLiteDatabase db = getWritableDatabase();
        int deletedUserId = db.delete(TABLE_NAME,USER_ID+" = ?",new String[]{String.valueOf(userId)});
        db.close();
        return  deletedUserId;
    }
    public ArrayList<UserModel>getUserList()
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel>list = new ArrayList<>();
        String query = "select " +
                "UserId," +
                "Name," +
                "FatherName," +
                "SurName," +
                "Gender," +
                "Dob," +
                "PhoneNumber," +
                "EmailAddress," +
                "TblMstLanguage.LanguageId," +
                "TblMstCity.CityId," +
                "LanguageName," +
                "CityName from TblUser inner join \n" +
                "TblMstLanguage on TblUser.LanguageId = TblMstLanguage.LanguageId" +
                " inner join TblMstCity on TblUser.CityId = TblMstCity.CityId";
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToNext();
        for (int i =0;i<cursor.getCount();i++)
        {
            list.add(getModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
    public ArrayList<UserModel>getUserByGender(int gender)
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel>list = new ArrayList<>();
        String query = "select " +
                "UserId," +
                "Name," +
                "FatherName," +
                "SurName," +
                "Gender," +
                "Dob," +
                "PhoneNumber," +
                "EmailAddress," +
                "TblMstLanguage.LanguageId," +
                "TblMstCity.CityId," +
                "LanguageName," +
                "CityName from TblUser inner join \n" +
                "TblMstLanguage on TblUser.LanguageId = TblMstLanguage.LanguageId" +
                " inner join TblMstCity on TblUser.CityId = TblMstCity.CityId"+
                " where "+
                " Gender = ?";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(gender)});
        cursor.moveToNext();
        for (int i =0;i<cursor.getCount();i++)
        {
            list.add(getModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
}


