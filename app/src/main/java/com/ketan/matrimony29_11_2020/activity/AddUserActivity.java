package com.ketan.matrimony29_11_2020.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;

import com.ketan.matrimony29_11_2020.R;
import com.ketan.matrimony29_11_2020.adapter.CityAdapter;
import com.ketan.matrimony29_11_2020.databases.TblMstCity;
import com.ketan.matrimony29_11_2020.model.CityModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUserActivity extends BaseActivity {
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etFatherName)
    EditText etFatherName;
    @BindView(R.id.etSurName)
    EditText etSurName;
    @BindView(R.id.rbMale)
    RadioButton rbMale;
    @BindView(R.id.rbFemale)
    RadioButton rbFemale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.etDob)
    EditText etDob;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    CityAdapter cityAdapter;
    ArrayList<CityModel> cityList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_user_activity);
        ButterKnife.bind(this);
        setSpinnerAdapter();
    }

    void setSpinnerAdapter()
    {
        cityList.addAll(new TblMstCity(this).getCityList());
        cityAdapter = new CityAdapter(this,cityList);
        spCity.setAdapter(cityAdapter);

    }

    @OnClick(R.id.rgGender)
    public void onRgGenderClicked() {
    }

    @OnClick(R.id.etDob)
    public void onEtDobClicked() {
    }

    @OnClick(R.id.btnSubmit)
    public void onBtnSubmitClicked() {
    }
}
