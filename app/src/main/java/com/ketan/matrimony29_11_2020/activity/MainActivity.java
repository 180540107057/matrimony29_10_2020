package com.ketan.matrimony29_11_2020.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.ketan.matrimony29_11_2020.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.cvActRegistration)
    CardView cvActRegistration;
    @BindView(R.id.cvActUserList)
    CardView cvActUserList;
    @BindView(R.id.cvActSearchUser)
    CardView cvActSearchUser;
    @BindView(R.id.cvActFavorite)
    CardView cvActFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cvActRegistration)
    public void onCvActRegistrationClicked() {
        Intent intent = new Intent(MainActivity.this,AddUserActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActUserList)
    public void onCvActUserListClicked() {
    }

    @OnClick(R.id.cvActSearchUser)
    public void onCvActSearchUserClicked() {
    }

    @OnClick(R.id.cvActFavorite)
    public void onCvActFavoriteClicked() {
    }
}